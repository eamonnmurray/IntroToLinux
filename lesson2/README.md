Introduction to Linux - Lesson 2
================================

[Full lesson list](../README.md)

Contents
--------

1. [Editing files](#editing-files)
    - [pico and nano](#pico-and-nano)
    - [emacs](#emacs)
    - [vi and vim](#vi-and-vim)
2. [Bash Shell](#bash-shell)
    - [Environment](#environment)
    - [Scripting](#scripting)
3. [Remote Access](#remote-access)
    - [ssh](#ssh)
    - [Security and ssh keys](#security-and-ssh-keys)
    - [scp](#scp)
    - [rsync](#rsync)

Editing Files
-------------

While you can use a desktop text editor such as `gedit` to write text files,
source code or script files, it's very useful to be able to edit files directly
in the terminal. This is especially true when running on remote systems, such
as a HPC system, where you will often want to quickly modify job scripts to run
your calculations.

As you become more comfortable with Linux, you may find you prefer to work with
a terminal editor, as many people do. There are several text editors which can
run in the terminal, and typically people end up using either `vi` or `emacs`
due to the power of these editors once you become accustomed to them.

I would suggest you give both vi and emacs a try to start with, see which you
like better, and try to stick with it till you become proficient (which will
take a while). Both have a fairly steep learning curve, but this will pay off
in the long term. I'll only give a brief overview of them here. I am far more
familiar with `vim` so give a little more detail on this, but I encourage you
to try out `emacs` also.

### pico and nano

These are basic terminal text editors, with one or both being available on many
systems by default. `nano` is a more recent version of `pico` and often
starting `pico` will in fact launch `nano`.

These both work in the same. You can start them by typing the command name,
e.g. `nano` to open an empty document, or with a filename as `nano filename`.

Menu options are listed at the bottom of the screen, where e.g. `^X` indicates
you should type `ctrl+x` for that option.

### emacs

`emacs` is a powerful text editor with many features available. Modern versions
of `emacs` will automatically detect if a desktop environment is available and
if so launch a graphical program, and otherwise will run in the terminal. You
can force it to start in the terminal by launching it as `emacs -nw`.

`emacs` commands can be accessed by pressing different keyboard shortcut
combinations.

- To exit: `ctrl+x ctrl+c`.
- To open the help: `ctrl+h ?`.
- To see a list of keybinds: `ctrl+h b`.

The main `emacs` documentation is available as an `info` page rather than a
`man` page. Type `info emacs` to open it. If you prefer to use `less` to read
the manual, you can do `info emacs | less`. A more complete overfiew of the
features and usage of `emacs` is available at
[https://www.gnu.org/software/emacs/tour/]. A good way to get started is to
use the gui interface and menus to begin with and gradually learn the
shortcuts to the features you use most often. Emacs also offers a built-in
tutorial; to begin this follow the link on the starting page when you open
emacs.

### vi and vim

`vi` and its more recent modification `vim` (which is short for vi improved)
are powerful terminal text editors. Running `vi` on many modern systems will in
fact start `vim`. Many systems also have a graphical version called `gvim`
installed.

`vim` operates under several different modes. Here are some of the basic
commands.

- *Normal*: by default when `vim` is started, you will be in "Normal" mode
  in which you can navigate around the file and enter other commands.
    - If you are not in normal mode, you can enter it by pressing `escape`.
    - The arrow keys, pageup and pagedown should work by default on the
    `vim` versions installed on your systems.
    - `h,j,k,l` can also be used to move left, up, down, right, respectively.
    - `G` jumps to the end of the file.
    - `gg` jumps to the top of the file.
    - `100gg` jumps to line 100.
    - `:w` writes the file.
    - `:q` quits.
    - `:q!` quits even if the file has unwritten changes.
    - `:wq` writes the file and quits.
    - `u` undoes the last action.
    - `i` enters insert mode at the cursor.
- *Insert*: This mode allows you to enter text as in a normal text editor.
    - Press `escape` when you are done to go back to Normal mode so you can,
      e.g. save your changes.

You can start an interactive tutorial by launching `vimtutor` from the command
line.

Bash Shell
----------

There are often several different shells installed on a Linux system. `bash` is
probably the most common, and is typically the default for new users on any
system. `bash` is running in the terminal, and it interprets and executes the
commands we type in.

### Environment

#### User Configuration

On startup, such as when a new terminal is opened, or you connect to a remote
system over the network, `bash` will read several configuration files depending
on how it detects it is running.

- For login shells (shells where you are prompted to login when you start it),
  `bash` will read configuration options in `~/.bash_profile` or `~/.profile`
  if these exist. Note `~` is interpreted by the shell as the users home
  directory.
- For non-login shells (such as when you open the terminal emulator) `bash`
  will read configuration options in `~/.bashrc`.
- A lot of the time, for simplicity, many people will put all their
  configuration options in `~/.bashrc` and leave their `~/.profile` empty
  except for a command to read the `~/.bashrc` file.

These files can be used to configure many aspects of the shell, such as how the
prompt looks in the terminal.

#### Environment Variables

Many important aspects of how `bash` behaves are governed by environment
variables. These can be modified to your preference in your configuration
files. To see the current value of one of these variables you can do, e.g.
`echo $PATH`. A `$` symbol is used when referring to the data stored in a
variable, and `echo` is a built-in bash command that outputs something to the
terminal.

- `PATH`: this tells bash which directories to look in for executables so they
  can be run without typing in their full path. For example, we can just type
  in `gedit` in the terminal to launch the program, as the `PATH` variable
  contains the directory holding an executable of this name. Different
  directories are separated by ":".
    - To add the directory `~/.bin` to your path, you can add the line
      `export PATH="~/bin:$PATH"` to the `.bashrc` file. Here the `export`
      statement is used to allow the variable to be usable by any child
      processes rather than local to the script. Note when we are naming the
      variable we want to assign a value to we don't use a `$`. Also we just
      want to add a directory to the existing `PATH` so we add `:$PATH` to keep
      all the existing directories.

### Scripting

Shell scripts allow you easily string together sets of commands in a useful
way. For example, you could write a script that runs a calculation, parses the
important results to another file, and then generates a plot.

This course will only be able to briefly cover scripting. If you're
interested in developing more advanced scripts, I strongly suggest referring
to the
[Advanced Bash-Scripting Guide](http://tldp.org/LDP/abs/html/index.html).

#### The basics

A simple example script to output "Hello World!" is as follows:

```bash
#!/bin/bash
# Simple example script that outputs "Hello World!"

echo "Hello World!"
```

- The first line tells the system what command is used to interpret the
  contents of the script. This line should always begin with `#!` and then the
  path to the executable. For `bash` the executable will almost always be
  `/bin/bash`.
- Comments being with `#`.
- Commands can be entered otherwise as you would enter them to the terminal.
- Try creating a file called `hello.sh` with the contents listed above.
- You will need to make the script executable with `chmod u+x hello.sh`.
- Then you can run it by typing `./hello.sh`.

#### Variables

Variables don't need to be declared in any way, you can assign a value and
start using them. For example:

```bash
#!/bin/bash
# Example using variables.

var1="Hello"
var2="World!"

echo $var1 $var2
# Note the use of the $ symbol when we want to use the value stored in the
# variable.

# Any command can use these variables
dirname="tmpdir"
mkdir $dirname
```

You can also read a value from stdin using the `read` command as follows:

```bash
#!/bin/bash
# Example showing how the read command is used.

echo "Please enter some text:"
read user_text

echo "The text you entered was:" $user_text
```

##### Capitalization

Note: variable names are case sensitive in bash. The usual convention used is
that environment variables (such as `PATH`), and internal shell variables
(such as `BASH_VERSION`) are capitalized, while other variables are in
lowercase. Adopting this convention will ensure that you don't accidentally
overwrite any important variables in your scripts.

#### Command Substitution

Command substitution allows the result of a command to replace the command
itself, acting much like a variable in practice. For example:

```bash
#!/bin/bash
# Example of command substitution.

# This can be done by enclosing the command in $( )
echo "It is now $(date)."
# Note: the "date" command outputs the current date and time.

# This can also be done by enclosing the command in backtics ` `
echo "The files in this directory are:" `ls`
```

#### Conditional Statements

`if` statements can be used in bash, and many types of tests are possible.
You can test if the value stored in a variable equals something as follows:

```bash
#!/bin/bash
# Example using if statatements

echo "Please enter a yes or no: "
read user_response

# Note the spaces following `[` and before `]` are important.
if [ $user_response = yes ]
then
    echo "You entered yes."
elif [ $user_response = no ]
# "elif" is the same as "else if"
then
    echo "You entered no."
else
    echo "You didn't enter yes or no."
fi
# "if" statements are always ended with "fi".
```

We can also check, e.g., if a file or directory exists:

```bash
#!/bin/bash
# Check if the directory "tmpdir" exists, and if not, create it, then check
# if the file "tmpdir/testfile" exists, and if not, create it.

dirname="tmpdir"
filename="testfile"

if [ ! -d "$dirname" ]
# The "!" is logical negation
# -d tests that a file exists and is a directory.
then
    mkdir $dirname
fi

if [ ! -f "$dirname/$filename" ]
# -d tests that a file exists and is a regular file (i.e. not a directory).
then
    touch "$dirname/$filename"
fi
```
Many other tests of this form are possible.

Note, bash can also use the `[[ ... ]]` test construction rather than
`[ ... ]`, with the former offering some more options and functioning more like
other programming languages, though it won't work in many other (non-bash, or
older bash version) shells.

#### Arithmetic and Testing

To test numeric values, the `(( ... ))` construction can be used. For example:

```bash
#!/bin/bash
# Example showing the use of the (( )) construction

var1=4
var2=5
var3=8

if (( var1 + var2 > var3 ))
# Note within the (( )) don't use $ symbols before variables.
then
    echo "$var1 + $var2 > $var3"
else
    echo "$var1 + $var2 <= $var3"
fi

# We can also use the construction to perform basic arithmetic
var4=$(( var1 * var3 ))
echo "$var1 * $var3 = $var4"
```

#### For Loops

For loops iterate a variable over a range of values. For example:
```bash
#!/bin/bash
# Simple for loop example.

# This construction loops over a space separated list of values. A variable
# whose contents contains spaces would work in the same way.
for i in 1 2 3
do
    echo "Iteration number $i"
done
```

A for loops can be used to apply a command repeatedly to a set of files. For
example:

```bash
#!/bin/bash
# Example showing a simple for loop over a list of arguments used to convert
# a set of data files with comma separted columns to tab separated columns.

for csvfile in "$(ls *.csv)"
do
    datfile="$(basename $csvfile .csv).dat"
    # basename is a tool to strip the suffix from a file. Here we use it
    # to construct a new filename with the .dat extenstion.
    sed 's/,/\t/g' $csvfile > $datfile
done
```

Bash also supports numeric loop ranges using the syntax
`{start..finish..increment)`. For example

```bash
#!/bin/bash
# Example of for loop with numeric increments

for i in {0..6..2}
do
    echo "The value of i is $i"
done
```

#### While Loops

Bash also supports while loops, where a set of commands are repeated
continuously while a given condition is true. For example:

```bash
#!/bin/bash
# Example of while loop in bash

counter=0

# Here -lt means less than
while [ $counter -lt 10 ]
do
  echo $counter
  counter=$((counter + 1))
done
```

It is often convenient to use `while` loops to operate on every line of a file.
For example:

```bash
#!/bin/bash
# Example of while loop reading lines from a file

linenumber=0

while read line
do
  linenumber=$((linenumber + 1))
  echo "$linenumber: $line"
  # This will prepend each line of a file with its line number.
done < test.txt
```

#### Arguments

We can also pass arguments to bash scripts from the command line. For example:

```bash
#!/bin/bash
# Example using command line arguments. Call this script with several
# arguments.

echo "The first argument is $1"
echo "The second argument is $2"
echo "The number of arguments is $#"
echo "The full list of arguments is $@"
```

We can loop over arguments using a for loop as follows:

```bash
#!/bin/bash
# Example using a for loop to iterate over command line arguments.

for arg in $@
do
  echo $arg
done
```

#### Functions

We can also define functions in bash as we would in other languages. These
allow sections of code to be reused as needed and can help make a script easier
to follow.

A simple example is as follows:
```bash
#!/bin/bash
# Simple example of a function in a bash script

# First we define a function called "hello", that will output "Hello World!"
# when called.
hello() {
  echo "Hello World!"
}

# To call the function, we just use its name.
# Note function names in the script will take precedence over executable names
# from the path in the script.
hello
```

Remote Access
-------------

Once you have become comfortable with the command line, you can start to take
advantage of one of the more useful characteristics of Linux: how easy it is to
work with remote machines. Working with machine in a server room thousands of
miles away can be almost as easy as working with a machine sitting on your
desk.

### ssh

The most common way you'll interact with remote systems is by connecting
to a shell on the remote machine using `ssh`, which stands for *secure shell*.

- `ssh username@exampleserver.ac.uk` will attempt to open a connection to the
  remote machine at exampleserver.ac.uk. In most cases, the remote server will
  prompt you for your password, and if accepted it will open a login shell for
  you. Note if your username is the same on the local machine as the remote
  machine, you can omit it: `ssh exampleserver.ac.uk`.

You can set up a configuration file containing details of servers you connect
to frequently, as well as many other options, in `~/.ssh/config`. See `man
ssh_config` for more details.

#### Useful Options

- `-Y` will enable X11 forwarding. You can use this to forward graphical output
  from the server to your local machine (provided the server is configured to
  allow it). Note: this will often be quite slow for all but the fastest
  connections. Typically you'll be better off copying data back to your local
  machine and using whatever graphical software you need there, or if some form
  of remote desktop is available this is usually much better too.
- `-p` allows you to specify a port number for the connection. By default ssh
  uses port 22, but some servers opt to use a non-default number, typically to
  reduce log spam from brute force hack attempts.
- `-vv` gives detailed output for the connection and is useful for when your
  connection has failed and you want to understand why.
- `-i` allows you to specify the identity file. You'll need to use this if you
  have generated an ssh key with a non-standard name (ssh keys are covered in
  the next section).

### Security and ssh keys

As always when setting up a Linux account, on both local and remote systems, it
is important to select a strong password. If you recycle a password that you
use for some other service, you could compromise the security of the entire
system.

To avoid users needing to set and remember strong passwords, many systems will
restrict connections to use only ssh keys. Even on systems where they don't, it
is still good to set up and use ssh keys, as it will make the process of
connecting to remote servers much easier, and more secure. Rather than
needing to remember the passwords for several different systems, you can
use the same ssh-key which is much harder to crack than a password, and
encrypted locally, so that you just need remember one encryption passphrase.

When you generate an ssh key, it creates a private key file and a public key
file.

- The private key file should never be shared, and should be encrypted with a
  strong passphrase when generated; if not, if someone gets access to your
  private key they will be able to gain access to any system on which you have
  enabled it.
- The public key can be shared, and it is this that will be copied to the
  remote machine.

To generate an ssh key pair, you can use the `ssh-keygen` command. If you
haven't created a key before, I would suggest keeping the default file name and
location; otherwise you will need to specify the identity file when you connect
with ssh as mentioned in the previous section (this can be specified in the ssh
config file).

- If you want to add or change passphrase for a key you have generated, you can
  do `ssh-keygen -p -f keyfilename`.
- The public key (`id_rsa.pub` by default) should be appended to
  `~/.ssh/authorized_keys` on the remote machine. You can usually do this by
  e.g. `ssh-copy-id exampleserver.ac.uk` if your key has the default name.

Once your key is created and placed on the remote system, when you attempt to
access the remote system, you will be asked for the passphrase to your key
rather than the server password, and the connection will then open.

Many systems will automatically cache the unlocked key while the will the
desktop session is open (modern Mac systems will do this by default). This
means you will only need to type the passphrase once, for the initial
connection, and subsequent connections will be automatically authenticated.

#### ssh-agent and ssh-add

You can explicitly cache the unlocked key for a bash shell by typing

```bash
ssh-agent bash
ssh-add
```

`ssh-agent bash` will start a bash shell which can store an unlocked key.
`ssh-add` will prompt you for your passphrase, and unlock your key. Once you
have done this, subsequent ssh connections within that shell will be
automatically authenticated.

For more details on ssh-keys, the [Arch Wiki
Page](https://wiki.archlinux.org/index.php/SSH_keys) has some good information.

### scp

`scp` stands for secure copy, and allows you to copy files to or from a remote
server over ssh. Here are some example uses:

- `scp file1 user@exampleserver.ac.uk` will copy `file1` in the current
  directory to the user's home directory of the remote server.
- `scp file1 user@exampleserver.ac.uk:path/to/store/` will copy `file1` in the
  current directory to the `/path/to/store` subdirectory of the user's home
  directory of the remote server.
- `scp user@exampleserver.ac.uk:file1 .` will copy `file1` from the home
  directory on the remote server to the current directory on the local machine.
- `scp -r user@example.ac.uk:/example/dir1 .` will recursively copy the
  directory `/example/dir1` from the remote server to the current directory of
  the local machine.

### rsync

`rsync` is a more powerful and versatile file copying tool than `scp`. It can
synchronise a local and remote directory fully, only copying files to the
target machine which are absent or older than than those on the source. File
attributes can also be maintained. Here are some typical usage examples:

- `rsync -auvz localdirectory user@remoteserver.ac.uk:` will copy a local
  directory to the users home directory on the remote server. The options used
  mean the following:
    - `-a` means *archive* mode. This sets several other options, including
      recursive copy (for directories), copy links as links, preserve
      permissions, modifications, and ownership.
    - `-u` means *update* mode. This skips files that are newer on the
      recieving system.
    - `-v` means *verbose* mode. This lists the file names as they are copied.
    - `-z` means compress files during the transfer. This automatically reduces
      the amount of data that needs to be copied.
- `rsync -auvz user@remoteserver.ac.uk:/example/directory .` will copy the
  remote directory to the current directory on the local system. The same
  options as before are used.

