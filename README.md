Introduction to Linux
=====================

NOTE
----

This material is no longer updated. It has been incorporated into the longer
[Introduction to Scientific Computing](https://gitlab.com/eamonnmurray/IntroToScientificComputing)
repository as three sections on Linux.

Preface
-------

This is a brief overview intended to get incoming TSM CDT Masters students
started using Linux. As well as covering some useful commands, and basic shell
scripting, this course will also touch on the git version control system,
plotting, and preparing documents in LaTeX. Note: some of the examples may
include extra details relevant to the Xubuntu install the students receive, but
otherwise the material is quite general.

Additional Resources
--------------------

- There are various useful [guides at the Linux Documentation Project](http://www.tldp.org/guides.html),
  in particular:
    - [Introduction to Linux](http://www.tldp.org/LDP/intro-linux/html/index.html)
    - [Bash Guide for Beginners](http://www.tldp.org/LDP/Bash-Beginners-Guide/html/index.html)
- Once you want to start thinking about more advanced scientific computing,
  Victor Eijkhout's free textbook [Introduction to High-Performancec Scientific
  Computing](http://pages.tacc.utexas.edu/~eijkhout/istc/istc.html) is an
  excellent resource.

Contents
--------

### [Lesson 1](lesson1/README.md)

1. [Using the Terminal](lesson1/README.md#using-the-terminal)
2. [Files and Directories](lesson1/README.md#files-and-directories)
    - [ls](lesson1/README.md#ls)
    - [pwd](lesson1/README.md#pwd)
    - [cd](lesson1/README.md#cd)
    - [touch](lesson1/README.md#touch)
    - [mkdir](lesson1/README.md#mkdir)
    - [rm](lesson1/README.md#rm)
    - [rmdir](lesson1/README.md#rmdir)
    - [ln](lesson1/README.md#ln)
    - [df](lesson1/README.md#df)
    - [du](lesson1/README.md#du)
    - [quota](lesson1/README.md#quota)
3. [Tab Completion](lesson1/README.md#tab-completion)
4. [Permissions](lesson1/README.md#permissions)
    - [chmod](lesson1/README.md#chmod)
5. [Reading Files](lesson1/README.md#reading-files)
    - [cat](lesson1/README.md#cat)
    - [head](lesson1/README.md#head)
    - [tail](lesson1/README.md#tail)
    - [more](lesson1/README.md#more)
    - [less](lesson1/README.md#less)
6. [The Manual and man command](lesson1/README.mdthe-manual-and-man-commands)
7. [Wildcards](lesson1/README.md#wildcards)
8. [IO Redirection](lesson1/README.md#io-redirection)
9. [History](lesson1/README.md#history)
10. [Other Useful Commands](lesson1/README.md#other-useful-commands)
    - [grep](lesson1/README.md#grep)
    - [awk](lesson1/README.md#awk)
    - [sed](lesson1/README.md#sed)
    - [tr](lesson1/README.md#tr)
    - [find](lesson1/README.md#find)
    - [diff](lesson1/README.md#diff)
    - [sort](lesson1/README.md#sort)
    - [uniq](lesson1/README.md#uniq)
    - [tar](lesson1/README.md#tar)
    - [bc](lesson1/README.md#bc)
11. [Job Control](lesson1/README.md#job-control)

### [Lesson 2](lesson2/README.md)

1. [Editing files](lesson2/README.md#editing-files)
    - [pico and nano](lesson2/README.md#pico-and-nano)
    - [emacs](lesson2/README.md#emacs)
    - [vi and vim](lesson2/README.md#vi-and-vim)
2. [Bash Shell](lesson2/README.md#bash-shell)
    - [Environment](lesson2/README.md#environment)
    - [Scripting](lesson2/README.md#scripting)
3. [Remote Access](lesson2/README.md#remote-access)
    - [ssh](lesson2/README.md#ssh)
    - [Security and ssh keys](lesson2/README.md#security-and-ssh-keys)
    - [scp](lesson2/README.md#scp)
    - [rsync](lesson2/README.md#rsync)

### [Lesson 3](lesson3/README.md)

1. [Version Control with Git](lesson3/README.md#version-control-with-git)
2. [Plotting](lesson3/README.md#plotting)
    - [Gnuplot](lesson3/README.md#gnuplot)
    - [xmgrace](lesson3/README.md#xmgrace)
3. [Visualizing Atomic Structures](lesson3/README.md#visualizing-atomic-structures)
    - [VESTA](lesson3/README.md#vesta)
4. [Document preparation with LaTeX](lesson3/README.md#document-preparation-with-latex)

