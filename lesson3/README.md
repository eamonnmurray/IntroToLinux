Introduction to Linux - Lesson 3
================================

[Full lesson list](../README.md)

Contents
--------

1. [Version Control with Git](#version-control-with-git)
2. [Plotting](#plotting)
    - [Gnuplot](#gnuplot)
    - [xmgrace](#xmgrace)
3. [Visualizing Atomic Structures](#visualizing-atomic-structures)
    - [VESTA](#vesta)
4. [Document preparation with LaTeX](#document-preparation-with-latex)

Version Control with Git
------------------------

A version control system allows a full history of changes, additions and other
modifications to a project to be maintained.

`git` is a distributed version control system. This uses a peer-to-peer system
as opposed to the client-server approach used by other version control systems.
This means that every working copy of the project is a full repository with the
complete project history and branches. For an outline of what this means in
practice, I suggest looking at the [Wikipedia page for Distributed version
control](https://en.wikipedia.org/wiki/Distributed_version_control).

`git` is one of the most popular version control systems, particularly in the
world of open source software. Most notably, it is used for development of the
Linux kernel. In this section we'll summarize and give some basic examples of
some of the more common operations involved in working with a git repository.

### `git help`

- This will give you more information about git and its subcommands and
  concepts.
- `git help merge` will open the git manual for the `merge` command. (The same
  page can be opened by typing `man git-merge`).
- `git help tutorial` will open a git tutorial. (The same page can be opened
   by typing `man gittutorial`). It his highly recommended to read through
   this, as it goes into much more detail of how git works than the overview
   presented here.
- `git help -g` will list the other guides that are available in the manual.

### `git config`

- This is used to set and read various git options for the repository, globally
  (for all projects under a given Linux user account), or for the entire
  system.
- One of the first things you'll need to do is set your username and email
  address so that it will be used on all your projects. This allows people
  to see who made a particular modification.
    - `git config --global user.name "Jane Doe"` will set your name.
    - `git config --global user.email "j.doe@example.com"` will set your email.
    - Once this is set with the `--global` option, git will be able to use this
      information any time you are working under this Linux user account.

### `git init`

- This is used to initialize a new git repository.
- Run this command to in the directory that contains, or will contain, the
  project that you want git to track.

### `git clone`

- This is used to make a copy of an existing repository into a new directory.
- `git clone https://gitlab.com/eamonn.murray/IntroToLinux.git` will download a
  copy of the repository holding this course from gitlab into a directory
  called IntroToLinux
- `git clone https://github.com/vhf/free-programming-books.git` will download a
  repository listing freely available programming material.

### `git add`

- You can use this to tell git that you want it to track certain files. The
  current version of files that are tracked will be saved in the git repository
  every time a new "commit" is performed (explained below). If you were making
  a C++ project, you should add all your source files, for example. But you
  shouldn't add the actual executable or object files.
- `git add README.md` will tell it to start tracking the file `README.md`.
- `git add SomeDirectory` will track the directory and all files in it.
- This also tells git that any changes to a tracked file should be incorporated
  in the next commit i.e. it is added to the "index" to use git terminology.

### `git commit`

- This tells git to store the current set of changes (i.e. the index) in a new
  commit. A commit saves the current state of the tracked files in your local
  repository (i.e. it would not affect the remote version you may have cloned
  from), and you will easily be able to revert back to that version if you wish
  later on. You should do this whenever you finish adding some functionality or
  fixing some bug etc.
- `git commit -am "Commit Message"` is the typical usage.
    - The `-a` flag adds all changes in tracked files to the index. This means
      you don't need to manually do a `git add` for these files.
    - The `-m` flag lets you set the commit message.
- The commit message will be be visible in the log for this commit.
- Writing a good commit message is very important. The git commit manual
  suggests the following:
  > it’s a good idea to begin the commit message with a single short (less than
  > 50 character) line summarizing the change, followed by a blank line and
  > then a more thorough description. The text up to the first blank line in a
  > commit message is treated as the commit title, and that title is used
  > throughout Git.

- You can write commit messages like this in the terminal. When you write the
  first `"` for the commit message, you can press enter to skip lines as
  needed until you write the closing `"`. So it would look like the following:

```bash
$ git commit -am "Commit title shorter than 50 characters
>
> Longer commit message. It's usually recommended to break lines at
> around 72 characters."

```
- There's more discussion and advice on writing good commit messages at
  http://chris.beams.io/posts/git-commit/

### `git log`

- This will show the commit logs. Usually `short` format is used, where only
  the "title" of the commit is listed.
- `git log --format=full` will give full details for each commit.

### `git branch`

- `git branch` without options will default to listing all branches in the
  repository, with the currently active branch marked with an asterix.
- `git branch mybranch` will create a new branch named "mybranch" starting from
  the currently active branch. Note the newly created branch will not be
  active.
- `git branch mybranch sourcebranch` will create "newbranch" starting from
  "sourcebranch".
- `git branch -d mybranch` will delete the branch "mybranch". It's good
  practice to delete branches once they have served their purpose and their
  changes have been merged into the master branch.

### `git checkout`

- `git checkout filename` will replace the file `filename` with the most
   recently committed version of that file. This is useful if you accidentally
   delete or overwrite a file.
- `git checkout branchname` will switch to working on the branch "branchname".
  This will update files in the working directory, and changes and commits will
  go to this branch.
- `git checkout -b bugfix` will create a new branch called bugfix and then
  check it out. It is equivalent to `git branch bugfix` followed by `git
  checkout bugfix`

### `git fetch`

- This will download changes, such as branches pending commits, from a remote
  repository. Your working copy will not be affected. By default, unless you
  specify a different remote, it will download from the origin remote.

### `git merge`

- This will incorporate changes from a set of commits (such as from another
  repository or another branch) into the current branch. Git will often be able
  to do this automatically, in which case it will generate a new commit
  representing the merged branches. However if the two branches being merged
  have modified the same part of the same file, git will stop before committing
  the merge and ask you to manually resolve the conflict(s). Git will list the
  files containing conflicts. You can use whatever editor you like to fix the
  file. Then when you are done, used `git add filename` to tell git the
  conflicts have been resolved in this file. Once all conflicting files are
  fixed, you can do `git commit` to generate the merge commit.

### `git rebase`

- This is another way to combine changes from two different branches that have
  diverged.
    - For example, say you have made a branch 'newfeature' and made several
      commits in that branch.
    - At the same time, you have also made several commits in the master
      branch to fix some bugs.
    - If we don't want to fully merge the newfeature branch into the master
      branch yet, we can instead do a rebase. This will take the changes
      you have made in each commit of the newfeature branch, and apply
      them to the most recent commit in the master branch.
- If you are in the newfeature branch (via `git checkout newfeature`), then
  `git rebase master` will apply the changes introduced in that branch to
  most recent committed version of the master branch.

### `git pull`

- This is equivalent to `git fetch` followed by `git merge`. Any updates are
  downloaded from the server and applied to your working copy.

### `git push`

- This will push commits made in your local repository to a remote repository.
- This can also take two arguments: the remote name, and the branch name. The
  default is equivalent to `git push origin master`.
- Sometimes you can get an error when your local repository is behind the
  remote repository. In this case, you should first do a `git fetch` before you
  can push.

### `git status`

- This shows the status of your working copy, such as files which have been
  modified since the last commit, aswell as files which are not tracked by the
  git repository.

### `git show`

- This will show the changes introduced by the most recent commit.

### `git diff`

- This will show the differences between your working copy and the most recent
  commit.

### `git tag`

- This is usually used to mark specific commits as important, such as for
  marking release versions (v1.0, v1.5, etc).
- Writing `git tag` with no further options will list all the tags in the
  repository.
- To create a tag you can write e.g. `git tag v1.5`. This will point to the
  current commit.
- You can also create an annotated tag using the `-a` flag, along with a
  message using the `-m` flag.
    - `git tag -a v2.0 -m "Release version 2.0"`
- Note: tags are not pushed by default when you do `git push`. To explicitly
  push tags you can write `git push origin --tags`.

### `git gui`

- This will launch a graphical user interface to git, that will allow you to
  use many of the commands listed here.

### Online Hosting

There are several websites which offer online hosting of git repositories, with
many additional features. The most popular are:

- https://github.com
    - This is the largest host of open source code in the world. Free accounts
      allow you to host public repositories and contribute to other existing
      projects. Additionally free student accounts allow you to create private
      repositories.
- https://gitlab.com
    - This is very similar to github but offers free accounts with many more
      features than github, such as unlimited private repositories. The
      underlying code is also all open source.
- https://bitbucket.com
    - Along with git, bitbucket also supports the Mercurial VCS. Their free
      account also offers unlimited public repositories, and unlimited private
      repositories for up to five collaborators.

### A Worked Example

Go to the `git-example` directory. In this you'll find a bash script called
`word_freq.sh`, which takes a filename as an argument and returns a sorted
list of words in the file according to how many times they are used. We are
going to create a git repository to track this script and make some changes.

**NB If you have cloned the IntroToLinux repository, the simplest thing is to start
in a new directory somewhere that is not below the `IntroToLinux` directory.**
For example:
```bash
cp -r IntroToLinux/lesson3/git-example .
cd git-example
```
Our first version of the `word_freq.sh` script is as follows:
```bash
#!/bin/bash
# Generate an ordered list of words according to frequency in a text file.

tr -d [:punct:] < $1 | tr [:space:] "\n" | sort | uniq -c | sort -n
```

First we need to ensure your name and email are set in git (see the
`git config` section above). If you have already done this, you don't need
to repeat it.
```bash
git config --global user.name "Jane Doe"
git config --global user.email "j.doe@example.com"
```

Now let's initialize a repository in this directory:
```bash
git init
```

We want to track the script in git, so let's add it to the git repository:
```bash
git add word_freq.sh
```

Try looking at the output of `git status` and you'll see there are changes to
be committed since a new file has been added. Now let's commit this first
version of the file.

```bash
git commit -am "First version of script to generate a word count"
```

Now look at the output of `git status`, and `git log`.

Take a look at the output of the script by running it against this `README.md`
file, and piping the output into less:

```bash
./word_freq.sh ../IntroToLinux/README.md | less
```

So this seems reasonable, though our script is a little hard to follow
as one long line. Let's break it up and write a comment for each step.
Edit the file in your favourite text editor to the following. Note a line
ending in `\` in bash indicates the command continues on the next line.
Comment lines are skipped.

```bash
#!/bin/bash
# Generate an ordered list of words according to frequency in a text file.

tr -d [:punct:] < $1 |\
  # Delete all punctutation.
  tr [:space:] "\n" |\
  # Convert spaces to newlines.
  sort |\
  # Sort alphabetically.
  uniq -c |\
  # Generate a count of how often each word appears.
  sort -n
  # Sort numerically.
```

Test it still works, and then we can commit this change. We'll give a longer
comment this time:
```bash
git commit -am "Reformat and comment each step

Break up the one long line into one line per piped command, and explain
what each step is doing with a comment."
```

Using `git log` will now show both commits. `git show` will show the changes
introduced by this last commit.

Looking at the output,  we see that we are counting words where a letter is
capitalized separately from where it all lowercase, such as 'The' and 'the'.
Let's make a new branch and try to fix this.

```bash
git checkout -b "Fix-case"
```

The fix for this is to add the `-i` flag to `uniq`. Update the script to make
this change. Test it to make sure it's working as expected then commit the
change to git with a suitable message.

```bash
git commit -am "Fix words with different case counted separately

Added the -i flag to the uniq command so words using different
capitalization would be counted together."
```

Now this branch has served it's purpose, we can merge it into the master
branch and delete it. Note typically branches are used for more than a single
commit before they are merged and removed.

```bash
git checkout master
git merge Fix-case
git branch -d Fix-case
```
Here we first switch to the master branch, merge the 'Fix-case' branch to
this. You'll see that this merge resolved automatically as a 'Fast-forward'
since the master branch hadn't changed since we created the 'Fix-case' branch.
The last command removes the branch as it's no longer needed.

Now, looking more carefully at the output, the most frequent word is actually
blank. What's happening here is `uniq -c` is counting the frequency of all the
empty lines. We don't care about these, so let's strip them out before we do
the sorting. We can do this with `grep -v "^$"`, where we use the special
characters `^` which matches the start of a line, and `$` which matches the end
of a line, so if `$` immediately follows `^` the line is empty.

- Make a new branch to fix this.
- Edit the script to add the suggested `grep` command following the second
  `tr`. Check it works as intended, then commit this change with a suitable
  comment.
- Merge it into the master branch and remove the branch you created.
- Other suggested changes are as follows. Try making each change and committing
  to the master branch each time.
    - Instead of stripping punctuation, convert punctuation to spaces, so that
      hyphenated words such as "follow-up" becomes "follow up" instead of
      "followup".
    - Reverse the sort order so the most frequently used words are listed
      first.
    - Limit the output to only the ten most frequent words.

Now say we want to track this in an online repository, for example GitLab:
- Make a GitLab account if you haven't already. You can sign in through e.g
  google or twitter if you don't want to remember a new login.
- Add your public ssh key (see [the ssh key section](#security-and-ssh-keys) if
  you haven't created one yet). This is accessible under the Profile Settings
  -> SSH Keys menu. This will allow you to push projects via ssh from the
  terminal.
- Click on the "New Project" button. This can also be accessed via the "+"
  button on the top right.
- Give the project a suitable name, and it's probably best to leave it private
  for now, and click the "Create Project" button.
- Now back in your terminal, you need to set your repository to point to this
  by typing `git remote add origin git@gitlab.com:username/projectname`,
  followed by `git push -u origin master`, to upload your repository. Note you
  will be asked for your ssh key passphrase.
- Now in future, whenever you do `git push` in this directory, the
  repository hosted on GitLab will be synchronized with your local repository.

Plotting
--------

It is important to be able to generate plots to show your results. There are
several software packages which can do this.

### gnuplot

Gnuplot is a command-line driven open-source plotting utility, with many
features such as fitting, and 3D plotting available. You can install it on
ubuntu systems by typing `sudo apt install gnuplot`. The homepage is
[gnuplot.sourceforge.net](http://gnuplot.sourceforge.net), and a detailed
manual for the latest release is [also
available](http://gnuplot.sourceforge.net/docs_5.0/gnuplot.pdf). Gnuplot is
also readily scriptable. This allows you, for example, to incorporate it into a
bash script to automatically produce a file containing a plot of your results
after your calculation has finished.

To open gnuplot, simply type `gnuplot` in a terminal. You will see some
information regarding the version of gnuplot that has started, and finally a
gnuplot prompt: `gnuplot> `. You can enter various commands here to generate
and save plots.

For example:
- `plot sin(x)`
    - This will plot the sin function. The x values will range from -10 to +10
      by default and the y range will be automatically chosen to be -1 to 1.
- `plot cos(x), x + 0.1*x**2`
    - This will plot the cosine function in addition to the function
      y=x-0.1*x^2 .
- `plot "plot/example.dat"`
    - Plots the values listed in the example file `plot/example.dat`.
- `set title "My Results"`
    - This sets a title for the plot.
- `set xrange [-1:1]`
    - Sets the range of the x-axis in the plot. `yrange` can be set similarly.
- `set xlabel "Position (pm)"`
    - Sets the label for the x-axis in the plot. `ylabel` can be set similarly.
- `replot`
    - After changing the plot by e.g. adding a title, it is necessary to redraw
      the output plot. The `replot` command repeats the last plot command.

#### Outputing to a file

To output a plot to for example a pdf file, you need to set the gnuplot
"terminal" appropriately (the terminal setting determines the type of output
generated by gnuplot), set an output filename, and redraw the plot. Typically
many different terminals are available which allow ouput to e.g. postscript,
png, gif formats.

For example, to save a default plot of a sin function to a pdf:

1. `set terminal pdf`
2. `set output "sin_plot.pdf"`
3. `plot sin(x)`

#### Fitting

We can also define and fit functions within gnuplot. For example, to fit a
quadratic to the example data in `plot/example.dat` we can do the following
(here I assume gnuplot has been started from within the `plot` directory):

- `f(x)=a+b*x+c*x**2`
    - This defines the function in terms of a set of parameters.
- `fit f(x) "example.dat" via a,b,c`
    - This will do a least squares fit, and output the final parameter values
      along with standard errors.
- Note if no initial values for the parameters are specified, gnuplot will
  start each at 1. You can specify initial values before running the `fit`
  command as e.g. `a=-1;b=-1;c=0.5`. It is particularly important to give good
  initial guesses when fitting non-polynomial functions.
- `plot "example.dat", f(x)`
    - This will generate a plot of the data points together with the fit curve.
      You can also use this to try to find good initial guesses for parameters
      manually when fitting more complex functions.
- A summary of the fit results is automatically saved in the file `fit.log`.

#### Scripting

One can create scripts as a list of gnuplot commands entered in the same way as
would be done manually. Then `gnuplot scriptname` will execute the script and
exit. An example script to perform a quadratic to the data in `example.dat` and
generate a pdf plot of the data compared with the fit is given in
`plot/example.gpl`:

```
f(x)=a+b*x+c*x**2
a=-1;b=-1;c=0.5;
fit f(x) "example.dat" via a,b,c
set title "Example Gnuplot Plot"
set xlabel "Position (Bohr)"
set ylabel "Energy (Hartree)"
set term pdf
set output "example-gp.pdf"
plot "example.dat" with lines title "Results", f(x) title "Quadratic fit"
```

Try entering the `plot` directory and running this as `gnuplot example.gpl`.
You will see information on the fit output directly to the terminal, and the
files `fit.log` and `example-gp.pdf` will be generated. You can view the pdf
with the `evince` document viewer application that is installed by default on
ubuntu systems: `evince example.pdf`.

### xmgrace

xmgrace is another popular linux plotting program. You can install this on
ubuntu systems by typing `sudo apt install grace`. By default it uses a more
graphical interface with menus allowing you to set all aspects of the plot,
although it can also be controlled via scripts.

Here is a worked example to generate a plot similar to that we created with
gnuplot, including the same quadratic fit:

1. Within the `plot` directory, type `xmgrace &` to start the application in
   the background.
2. Go to Data -> Import -> ASCII.
    - Click on `example.dat` under the "Files" heading (so that it is
      highlighted black).
    - Click "OK".
    - Click "Cancel" to close the import window.
3. Double click the x-axis. (Or go to Plot -> Axis Appearance, and ensure
   "X-axis" is selected in the "Edit" drop down menu.)
    - Change "Start" and "Stop" to 1.1 and 2.1.
    - Enter "Position (Bohr)" as the "Axis label" string.
    - Change the "Major spacing" to 0.1.
    - Click "Accept", to apply all changes and close the dialogue box.
4. Double click the y-axis. (Or go to Plot -> Axis Appearance, and ensure
   "Y-axis" is selected in the "Edit" drop-down menu.)
    - Change the "Start" and "Stop" to -1.07 and -1.02.
    - Enter "Energy (Hartree)" as the "Axis label" string.
    - Change the "Major spacing" to 0.005.
    - Click "Accept", to apply all changes and close the dialogue box.
5. Double click the plot line itself. (Or go to Plot -> Set Appearance).
    - Set the "Legend" string to "Results".
    - Click "Accept".
6. Go to Plot -> Graph Appearance.
    - Under "Title" enter "Example Xmgrace Plot".
    - Click "Accept".
7. To do a quadratic fit, go to Data -> Transformations -> Regression.
    - Select "Quadratic" in the "Type of fit" drop-down menu.
    - Click "Accept" to perform the fit (since we only have one data set at
      this point it is automatically selected). The results of the fit are
      shown in a pop-up window.
    - Click "Close" to close the regression window.
    - You should see that the fit curve has automatically been added to the
      plot.
8. Double click the red plot line. (or go to Plot - Set Appearance, and
   highlight the second set in the "Select Set" box.)
    - Set the "Legend" string to "Quadratic fit".
    - Click "Accept".
9. To output the plot, go to File -> Print Setup.
    - There isn't a pdf option by default, so instead select "EPS" from the
      "Device" drop-down menu.
    - Enter "example-xm.eps" under "Filename".
    - Click "Accept".
    - Go to File -> Print. This will output the plot to the specified file.
10. Let's also save the file in a format xmgrace understands. This will allow
   us to easily come back to it if we need to modify its appearance in future.
    - Go to File -> Save As, and under selection enter "example.agr" following
      the last "/".
    - Click "OK" to save the file.
11. Close xmgrace via File -> Exit.
12. We can examine the eps plot file we generated using the "gv" command
    - `gv example-xm.eps`.
    - We can also convert this easily to a pdf file by using the `epstopdf`
      utility. This can be installed on ubuntu systems through `sudo apt
      install texlive-font-utils`. Then you can do `epstopdf example-xm.eps` to
      generate the pdf.

Visualising Atomic Structures
-----------------------------

There are several utilities you can use to visualise crystal and molecular
atomic structures, such as `xcrysden`, `jmol` (both available through `apt`)
and `vmd`. In this section we'll give a brief example of using a more recently
developed utility called `vesta`.

### Vesta

There is a detailed pdf manual onlne which is linked at
[jp-minerals.org/vesta/en/doc.html](http://jp-minerals.org/vesta/en/doc.html).
We'll briefly describe how to install it under Linux and use it to look
at molecular and crystal systems.

#### Installing

The download page for VESTA is at
[jp-minerals.org/vesta/en/download.html](http://jp-minerals.org/vesta/en/download.html).
Go to this page and download the file `VESTA-x86_64.tar.bz2`. This can
be unpacked by typing `tar -xvf VESTA-x86_64.tar.bz2` (see the
[tar section](#tar) for reference). The executable is then called `VESTA`
in the unpacked directory. It may be useful to create a link to this
executable in your `~/bin` directory if you have added this to your path
(c.f the [bash environment section](#environment-variables)).

### Example - Water Molecule

Open VESTA and from the file menu, open the water molecule structure in
`structures/h2o-molecule/h2o.xyz`. An `xyz` file is one of many formats that
VESTA can load, where atomic coordinates of a molecule are listed explicitly in
Cartesian coordinates. You will see the oxygen atom as a red ball connected to
to hydrogen atoms as white balls.

- Using your mouse you can grab and rotate the structure around to view it from
  different angles.
- Tools are available on the left hand menu to calculate information on the
  structure such as the distance between two atoms, and the angle formed by
  three atoms.
- From the File menu, you can export an image of the current view either in
  "raster" formats (where an image is defined in terms of a grid of pixels,
  such as jpg or png), or "vector" formats (where an image is defined in terms
  of explicit lines, points and curves).

### Example - Crystalline Silicon

From the file menu, open the silicon structure in
`structures/build-silicon/POSCAR`. The POSCAR file format is used in the DFT
code VASP, and allows both the periodic lattice vectors and atomic basis of a
crystal to be defined.

- To add bonds to nearest neighbour silicon atoms, open the bonds menu under
  Edit -> Bonds.
    - Click New, set the Max. Length to 2.5. Ensure the option "Search
      additonal atoms if A1 is included in the boundary" is ticked.
    - Cick OK. The four atoms bonded to each atom on the boundary should now be
      shown.
- To expand the number of periodic images, open the boundary menu under Objects
  -> Boundary.
    - Increase x(max), y(max) and z(max) to 3. Atoms in a number of unit cells
      are now shown.
- When you are happy with how your structure is represented, you can save this
  in vesta's own file format (under the File menu), so you don't need to set
  e.g. the bonds and boundary next time you open it.

Document preparation with LaTeX
===============================

LaTeX is a document preparation system and markup language. It is very commonly
used to prepare scientific papers.

LaTeX offers several advantages over using a word processor:

- Formatting is handled automatically, so you can focus on content.
    - Many journals make LaTeX style files available so you can automatically
      format your work in the style appropriate to that journal: e.g. revtex
      for the American Physical Society journals.
- Mathematical equations are straight forward to generate.
- The LaTeX source file is plain text, so you can use whatever editor you like.

### Basic Example

```latex
\documentclass{article}

% Comment lines start with %.
% Latex commands start with a \.
% The first line in a latex document is always \documentclass, with the
% chosen class enclosed in braces. Many options are possible here, such as
% article, report, or book, and each will format the content in different ways.

\begin{document}
% The main content of a latex document is enclosed between a \begin{document}
% and a \end{document} command.

Paragraphs are indicated by separating them with an empty line in the source
file.

This paragraph includes some simple math: $f(x)=\frac{1}{2}x^3$.
% Inline math is enclosed betweeen two $ symbols.

\end{document}
```

#### Compiling

This file is included in the `LateX` directory as `example1.tex`. The most
straightforward way to generate a pdf document from this is to use the
`pdflatex` command. Try this now as `pdflatex example1.tex`.

### More Detailed Example

```latex
\documentclass[12pt,a4paper]{article}
% Additional class options can be given in brackets. Here we set the main
% font size, and the page size.

% This time we include some title information.
\title{A \LaTeX{} example}
\author{\'Eamonn Murray}
\date{\today}

\begin{document}

% The maketitle command will generate a title from the information defined
% in the header above.
\maketitle

% We can use the section command to divide the document into sections.
% subsection, and subsubsection can also be used to further subdivide
% sections.
\section{Introduction}

Equation \ref{geometric_sum} below shows another way to include mathematical
expressions. It is given a label so it can be referred to in this paragraph.

\begin{equation}\label{geometric_sum}
\sum_{i=0}^{\infty} z^k = \frac{1 - z^{n+1}}{1 - z}
\end{equation}

\section{Conclusion}

Conclusions go here.

\end{document}
```

### Compiling

This time if we run `pdflatex example2.tex` we will see among the output
```
LaTeX Warning: There were undefined references.

LaTeX Warning: Label(s) may have changed. Rerun to get cross-references right.
```

This is because we have labelled and referred to an item in the document. When
we do this we need to process the document a second time so that the items are
numbered correctly. If you look at the pdf that has been generated at this
point, instead of the item number there will be a ??. Rerun
`pdflatex example2.tex` and you will see this message no longer appears, and
the generated pdf now has the correct numbering.

### Including Graphics

Here's an example showing how to embed a plot we created earlier in a document.

```latex
\documentclass[12pt,a4paper]{article}

\usepackage{graphicx}
% We need to include the graphicx package which tells latex how to handle
% graphical files.

\title{A \LaTeX{} example including an image}
\author{\'Eamonn Murray}
\date{\today}

\begin{document}
\maketitle

Here we include the plot generated by gnuplot previously, where we have fit
the function $a + b x + c x^2$ to some example data.

\includegraphics{example-gp.pdf}

Note that what image formats can be used depends on what command you use to
compile the document. When using pdflatex, pdf, png and jpg files can all be
included.

\end{document}
```

Compile this with `pdflatex example3.tex` and examine the output using
`evince example3.pdf`.

### Physics

There is a ``physics`` package available for latex, that makes including
various common mathematical expressions in physics much easier. For more
details see the documentation on its
[CTAN page](https://www.ctan.org/pkg/physics?lang=en).

### Additional Resources

For more information on LaTeX, the best reference available is probably [The
Not So Short Introduction to LaTeX](https://tobi.oetiker.ch/lshort/lshort.pdf)
which is free online.
