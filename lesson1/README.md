Introduction to Linux - Lesson 1
================================

[Full lesson list](../README.md)

Contents
--------

1. [Using the Terminal](#using-the-terminal)
2. [Files and Directories](#files-and-directories)
    - [ls](#ls)
    - [pwd](#pwd)
    - [cd](#cd)
    - [touch](#touch)
    - [mkdir](#mkdir)
    - [rm](#rm)
    - [rmdir](#rmdir)
    - [ln](#ln)
    - [df](#df)
    - [du](#du)
    - [quota](#quota)
3. [Tab Completion](#tab-completion)
4. [Permissions](#permissions)
    - [chmod](#chmod)
5. [Reading Files](#reading-files)
    - [cat](#cat)
    - [head](#head)
    - [tail](#tail)
    - [more](#more)
    - [less](#less)
6. [The Manual and man command](the-manual-and-man-commands)
7. [Wildcards](#wildcards)
8. [IO Redirection](#io-redirection)
9. [History](#history)
10. [Other Useful Commands](#other-useful-commands)
    - [grep](#grep)
    - [cut](#cut)
    - [awk](#awk)
    - [sed](#sed)
    - [tr](#tr)
    - [find](#find)
    - [diff](#diff)
    - [sort](#sort)
    - [uniq](#uniq)
    - [tar](#tar)
    - [bc](#bc)
11. [Job Control](#job-control)

Using the Terminal
------------------

Modern Linux distributions typically automatically set up desktop environments,
such as Gnome, KDE, or XFCE. These allow you to do quite a lot without needing
to type commands into a terminal. However, becoming familiar with using the
terminal will allow you to access the full power of a Linux system, and is
particularly useful when working on systems remotely, such as when working with
high-performance computing clusters.

The terminal allows you to type in various commands and instructions directly.
Open the terminal application by clicking on the appropriate icon. On the
Xubuntu installs you have been set up with, you can find "Terminal Emulator"
listed in the main menu as shown in the image:

![xfce terminal emulator in the main menu](img/xfce_terminal_emulator.png)

Note it is also listed under the accessories category, and the shortcut
`alt+ctrl+t` will open the terminal emulator by default. You can usually
customize the appearance to your preference by changing the text size, font and
colour scheme.

You can launch a program from the terminal by typing its name. For example
typing `firefox` will launch the firefox web browser by default.

Sometimes commands can generate a lot of output in the terminal. If you want to
scroll back up most terminals allow you to move up and down as follows:
- `shift+uparrow`: scroll up one line.
- `shift+downarrow`: scroll down one line.
- `shift+pageup`: scroll up one page.
- `shift+pagedown`: scroll down one page.

Files and Directories
---------------------

Once you have opened the terminal, the first thing you need to be able to do
is navigate the file structure.

### ls

`ls` is used to list the contents of a directory. Try typing it now. By default
you should start off in your home directory when you open a terminal, so you
should see several automatically generated directories. Note `ls` has been set
to use colour highlighting for you by default, so different types of files are
shown in different colours.

#### Useful Options

- `ls -lh`. The `-l` option requests a long listing format, giving extra
  details about each file and folder, such as permissions, owner, size, and
  modification date. The `-h` requests sizes in a human-readable format,
  otherwise sizes are listed in bytes i.e. with this option `ls` would list
  "4.0K" instead of "4096".
- `ls -a`. This tells `ls` to list _all_ files, i.e. to not to ignore files
  starting with "." which are typically hidden by `ls` and often used to store
  various user configuration files. Try typing this now, and you'll see many
  more files and folders than previously.

### pwd

`pwd` will output the name of the present working directory. Try using it now.
It should respond with something like `/home/username`.

### cd

`cd` is used to change directory. You can specify either the full path, or a
directory relative to your current directory.

- `cd Documents` to move to the `/home/username/Documents` directory assuming
  we are in `/home/username`.
- `cd /home/username/Desktop` to move to the users `Desktop` directory
  regardless of where we start from.
- `cd ..` to move up one directory.
- `cd .` to move to the present directory (i.e. do nothing).
- `cd` and `cd ~` will both move to your home directory.

### touch

`touch` is mainly used to update a files timestamp (the time listed with
`ls -l` which is usually the most recent modification time). In
your home directory, try typing `touch Desktop` followed by `ls -l`, and you
will see the timestamp has been updated. However, the most common use for touch
is as a very quick way to create new, empty file. Try typing `touch tmp` in
your home directory. This will create a new empty file called `tmp`. This is
useful when a script or code requires a that a particular file exists.

### mkdir

`mkdir` is used to create new directories. For example `mkdir tmpdir1` will
create a new directory called `tmpdir1`.

#### Useful Options

- `mkdir -p` can be used to make parent directories as needed. For example,
  `mkdir -p tmpdir1/tmpdir2/tmpdir3` if `tmpdir1` already exists will create
  `tmpdir2` as a subdirectory of it, and `tmpdir3` as a subdirectory of
  `tmpdir2`.

### rm

`rm` is used to delete files. For example, if we create a file with
`touch tmp` (this will update its timestamp if it already exists), we can
delete it with `rm tmp`.

#### Useful Options

- `rm -r` will recursively delete a directory and any files contained in it.
- `rm -f` can be used to force deletion without prompting. Combine with the
  previous option with caution. There is no recycle bin when using `rm`.
  When you use it to delete a file it is gone, unless you have a backup.

### rmdir

`rmdir` is used to delete directories. Note this will give an error if the
directory you try to delete is not empty.

### cp

`cp` is used to copy files. For example, `cp sourcefile newfile` would copy
the file `sourcefile` to `newfile`.

#### Useful Options

- `cp -r` will recursively copy a directory and any files contained i it.

### ln

`ln` is used to create links. These are somewhat similar to shortcuts in
windows except that they operate at the filesystem level. A link to a file can
be used in the same way as the file itself. They are useful for many reasons:
- Convenience: you may want to frequently use a particular directory, but it
  has quite a long path. You can create a link to it in your home directory
  and use the link in exactly the same manner as the original path.
- Saving space: For example, when working with a code that expects a certain
  file as input in the current directory, you can make a link instead of
  copying the file.
- If a parameter file is used in several places, you can use links so that
  when you update when file, they are all updated, instead of having to update
  several copies manually.

`ln` can create two types of links: hard links are created by default, and
symbolic links are created when the `-s` option is specified. Symbolic links
contain text which points to a particular file, while hard links point to the
same data on the disk as the original file. It's generally simplest to stick to
symbolic links, especially starting out. An example of how links can be used is
as follows:

1. `mkdir tmpdir1; mkdir tmpdir2; touch tmpdir1/tmpfile1` This will create two
   directories and a file in one of them. Note semi-colons can be used to
   separate commands on the same line.
2. `cd tmpdir2` Move to the tmpdir2 directory.
3. `ln -s ../tmpdir1/tmpfile1 tmpfile2` This will create a symbolic link to the
   file.
3. `ln -s ../tmpdir1 .` will create a link to the `tmpdir1` directory in
   `tmpdir2`.
4. Typing `ls` in the `tmpdir2` directory will show a files `tmpfile2` and
   `tmpdir1`. If your colour highlighting is working, you should see these as
   different colours to usual files (probably light blue). To see where the
   links point you can use `ls -l`.

`cp -s` can also be used to create symbolic links (with `cp -l` creating
hard links), and can be a little easier to use, particularly for linking a
set of files.
- `cp -s ../results/*dat .` would create symbolic links from all the
   `.dat` files in the results directory to the current directory.

#### Useful Options

- `ln -sf`: the additional `-f` option forces the link to overwrite any
  pre-existing files or links.
    - For example, if we had incorrectly done `ln -s ../tmpdir1/tmpdir1 .` in
      the above example, the link would have been created, but wouldn't point
      to any valid file. `ls` will usually show these invalid links as red. We
      could then do `ln -sf ../tmpdir1 .` to overwrite the `tmpdir1` link with
      the correct path, rather than deleting incorrect link and repeating the
      `ln` command.

### df

`df` is used to report file system disk space usage for all mounted partitions.
This is a useful way to check how much space is free on the disks and
partitions used to store files by the system. It also tells you which disks are
are real local disks: these are the ones with labels like "/dev/sda2" under the
filesystem heading, while disks mounted across the network will be labelled
with the server address in this field. The "tmpfs" label indicates a temporary
filesystem, usually this is stored in RAM while the system is running, but can
be used through a directory in the file hierarchy.

#### Useful Options

- `df -h` will list usage numbers in a human-readable format, i.e. instead of
  10240, it would say "10M".
- `df -T` will list the filesystem type e.g. ext3/ext4 for disks using a
  typical Linux filestructure, and nfs for disks mounted remotely over the
  network.

### du

`du` is used to estimate file space usage. Typing `du filename` will tell you
how big `filename` is (usually in kB by default). `du dirname` will tell you
how big all the files in directory `dirname` and its subdirectories are, with a
total at the end.

#### Useful Options

- `du -s` will only print the total size used for the argument. This is
  typically used to find how much space a given directory is using.
- `du -h` will print sizes in human-readable format.

### quota

`quota` is used to check disk usage and limits. Many shared Linux systems, such
as HPC systems, will impose a limit on the disk space used by any individual
users. The `quota` command will tell you what those limits are and how much you
have used.

#### Useful Options

- `quota -s` will show limits and usage in a human-readable format, i.e.
  instead of 3145180 it would say 3072M.
- `quota -v` will show information on all filesystems, even if no storage has
  yet been allocated by the user on them.

Tab completion
--------------

A useful feature which has long been available in the Linux terminal is tab
completion. This is also referred to as command-line completion. These days
this is available in both Mac and Windows systems also.

Try typing `fir` in a terminal and hitting `tab`. The full name of the program
`firefox` should automatically fill in. If more than one match is possible,
pressing `tab` twice will bring up a list of possible matches.

This can also be used to complete file and directory names. From your home
directory, try typing `cd De` and pressing tab. It should automatically fill
in the full command to be `cd Desktop/`.

Permissions
-----------

If you take a look at the output of `ls -l` you'll see the first column has a
mix of letters (usually d, r, w, x, and hyphens).

- The first character in this column indicates the file: `d` for directories,
  and `-` for regular files.
- The following nine characters indicate the permissions. These are in sets of
  three where each set indicates the permission for a set of users.
    - The first set of three characters are the permissions for the user that
      owns the file (listed in the third column).
    - The second set of three characters are the permissions of other members
      of the group that own the file (listed in the fourth column).
          - The default on many Linux systems is to create a group of the same
            name as the username, that contains only that user.
          - It's also possible for users to be added to several groups
            by the system administrator. This is useful on shared systems
            where a certain set of users want to share access to some set of
            files, but without giving access to everyone.
          - You can see what groups you are in by typing `groups` in the
            terminal.
    - The third set of three characters are the permissions for all other
      users.
- Within each set of three characters:
    - The first character is "r" if users in that set have permission to read
      the file and "-" otherwise.
    - The second character is "w" if users in that set have permission to write
      to the file and "-" otherwise.
    - The third character is "x" if users in that set have permission to
      exectute to the file, i.e. run it as a program.

### chmod

`chmod` can be used to change file permissions. This command can be invoked in
two different ways:

One can change the permission for a given set of users granularly:
- `chmod u+x filename` grants the user who owns the file execute permission.
  This is one of the main things you will be using `chmod` for as when you
  create a script in a text editor it will not be executable by default.
- `chmod g+rw filename` grants group members read and write permission.
- `chmod o-r filename` revokes other users read permission.
- `chmod a+x filename` grants all users execute permission.

One can use a set of three numbers to set the full list of permissions at once.
What each number corresponds to is listed in following table:

|  #  | Permission              | rwx |
|:---:|:------------------------|:---:|
|  7  | read, write and execute | rwx |
|  6  | read and write          | rw- |
|  5  | read and execute        | r-x |
|  4  | read only               | r-- |
|  3  | write and execute       | -wx |
|  2  | write only              | -w- |
|  1  | execute only            | --x |
|  0  | none                    | --- |


- To set the permissions of a directory so only the owner can access it in any
  way you could use `chmod 700 directoryname`.
- To set a script you have created so that others can use and execute it you
  could use `chmod 755 scriptname`.

Reading a file
--------------

There are several built-in programs that will allow you to read through a text
file in the terminal.

### cat

`cat` is the simplest command to check the output of a file.
- `cat filename` will output the contents of `filename` to the terminal.
  For a large file you may need to scroll back up in the terminal as described
  in the section [Using the Terminal](#using-the-terminal).

### head

`head` outputs the first few lines of a file to the terminal.

- `head filename` outputs the first 10 lines of `filename`.
- `head -5 filename` outputs the first 5 lines of `filename`.

### tail

`tail` outputs the last few lines of a file to the terminal.

- `tail filename` outputs the last 10 lines of `filename`.
- `tail -5 filename` outputs the last 5 lines of `filename`.
- `tail -f filename` starts tail in "follow" mode, where tail will repeatedly
  check for new data written to the file, and output them to the terminal. This
  is useful for following the output from a running calculation for example.

### more

`more` works in a somewhat similar way to `cat`, except for files bigger than
the terminal window, it allows you to look through a page (screen) full of data
at a time.

- `more filename` to start paging through a file.
- To move to the next page press `space` or `z`.
- To move down one line of text press `enter`.
- To quit before reaching the end of the file press `q`.

### less

`less` is more advanced version of `more` with many more features. This doesn't
output file contents to the terminal however, so once you exit `less` your
terminal will be as it was before the file was opened.

- `less filename` to open a file.
- move down one line: `down-arrow`, `enter`, `j`, or `e`.
- move up one line: `up-arrow`, `y`, or `k`.
- move down one page: `page-down`, `space`, or `f`.
- move up one page: `page-up`, `w`, or `b`.
- search for a string of text: `/`, type in the search text and press enter.
- A useful feature when trying to check a file that is currently being written
  to is to press `F`. This is similar to `tail -F`. It will scroll forward and
  keep trying to read when the end of the file is reached. To exit this mode
  press `ctrl+c`.
- quit: `q`.

The Manual and man command
--------------------------

Linux systems provide an interface to a built-in reference manual through the
`man` command. This is very useful as a quick way to check the syntax used by a
command, and what options it might take.

- Type `man ls` to check the manual page for the ls command.
- Type `man man` to check the manual page for the man command.
- Type `man intro` to get an introduction to user commands.

When reading the manual data, by default it should use the `less` pager, so you
can use all the commands for `less` listed above to navigate and search through
the manual.

Wildcards
---------

In Linux many commands will accept wild cards as a way to perform their action
on a set of files.

- `*` is used to represent zero or more characters. The `*` wildcard is very
  commonly used and worth remembering.
    - `ls *.pdf` would list all the files ending in `.pdf` in the current
      directory.
    - `rm *.o` would remove all files ending in `.o` in the current directory.
- `?` is used to represent any single character.
    - `cp ../example?.cpp .` would copy all files with names such as
      `example1.cpp`, `example2.cpp`, `examplea.cpp` from the parent to the
      current directory.
- `[]` specifies a range.
    - `ls example_0[2,3,5]` would list files with the name `example_02`,
      `example_03` and `example_05`.
    - `ls example_0[3-5]` would list files with the name `example_03`,
      `example_04` and `example_05`.

For more information on this see `man 7 glob`.

IO Redirection
--------------

IO redirection allows you to easily send the output of a command to a file, use
a file as input for a command, or use the output of one command as input to
another command. The most frequently used features are as follows:

- `>` is used to redirect the output from a command into a file.
    - `ls > dirlist.txt` will save the result of the `ls` command in a file
      called `dirlist.txt`. **Take care:** if `dirlist.txt` already exists it
      will overwrite the previous contents.
- `&>` is used to redirect both the standard output and standard error from a
  command to the same file.
- `>>` is used to redirect the output from a command to a file, but will append
  the output to the end of the file if the file already exists.
    - If you run `ls >> dirlist.txt` you will see the ls output repeated
      several times in the file `dirlist.txt`.
- `<` is used to redirect a file to the input of a command.
    - `less < dirlist.txt` will work as `less` is happy to read either from
      stdin or a file.
- `|` is used to redirect the output of a command to the input of another
  command.
    - `ls | less` will take the output of the `ls` command and display it
      directly in `less` (this is why it's useful for `less` to be able to read
      from stdin).
- `tee` allows you to split a stream so that the output goes both to stdout and
  to a file.
    - `ls -l | tee dirfiles` will output the result of `ls -l` to the terminal
      and also save it in the file `dirfiles`.

History
-------

By default the terminal keeps a history of previously entered commands. You can
scroll back through previously entered commands using the `up-arrow` and
`down-arrow` keys.

The `history` command will output a list of all the previously entered commands
following a history index number. By default the list is usually limited to
1000 commands, with the oldest being removed as new commands are entered. You
can look at the full list with `history | less`.

- `!!` will repeat the most recent command.
- `!n` will repeat the command indexed by `n` in the history list. For example
  `!100` would repeat the command at history index 100.
- `!string` will repeat the most recent command in the history starting with
  "string".

For more details see `man history`.

The Linux shell also gives a built in way to search through previously entered
commands. You can press `ctrl+r` to bring up the reverse-history-search and
type some text from the command you want to re-enter. You can keep hitting
`ctrl+r` to search back through commands containing that text. If you
accidentally go past the entry you want, `ctrl+s` will go forward through
your history.

An additional useful shortcut is 'alt+.' which will enter the most recent
argument you used, and repeated presses will cycle back through arguments.


Other Useful Commands
---------------------

### wc

`wc filename` will output the newline, word and byte counts for `filename`.

#### Useful Options

- `-l` to output the number of lines in the file.
- `-w` to output the word count for the file.

### grep

`grep` will print lines from a file or stdin which match a given pattern.

- `grep searchtext filename` will output all lines in `filename` which contain
  the text `searchtext`.
- `history | grep less` will output all lines in the command history containing
  `less`. This is useful for those times when you entered a complex command
  some time ago that you want to repeat.

While `searchtext` in the first example above could be a particular word you
want to find an exact match of, `grep` will also interpret this as a *regular
expression* be default. This is somewhat similar to the wildcards you can use
in the terminal, but has a slightly different syntax and allows for much more
complex patterns.

#### Regular Expressions

This is a very deep topic, so we'll only cover a few of the more simple
examples. `man grep` has significantly more detail. The most useful symbols are
probably:

- `.` matches any single character.
- `*` the preceding item will be matched zero or more times.
- These are quite useful when combined to form `.*`, which acts in the same way
  as the terminal wildcard expression `*`.
- Note: to match the actual `.` or `*` symbols, you can escape them as `\.` and
  `\*`.

For example `grep "doc.*\.pdf" dirfiles.dat` will output all lines containing
strings that begin with `doc` and end with `.pdf`.

Note regular expressions can also be used in `less` (and hence `man`) when
searching for text with `/`.

#### Useful Options

- `grep -3 searchtext filename` will additionally output 3 lines before and
  after any lines containing `searchtext`. Any number of lines can be used
  here.
- `grep -v searchtext filename` will output all lines *except* those containing
  `searchtext`.
- `grep -r searchtext` will recursively search all files and folders starting
  from the current directory.

### cut

`cut` prints selected parts from each line a file. Mostly this is used with the
`-f` option which tells it to print only certain fields, and the `-d` option
which allows you to set the delimiter for fields (TAB is the default). It is
often useful to pipe (`|`) the output of `grep` into `cut` to parse data from
an output file.

For example, `cut -d ' ' -f 1 filename` will print the first word (separated by
spaces) on each line of `filename`, and `cut -d ',' -f '3-5' data.csv` would
print the 3rd, 4th and 5th columns of a csv data file.

#### Useful Options

- `-s` tells `cut` not to output lines which do not contain delimiters. This is
  useful for example if you have empty lines in the file you are parsing that
  you want to suppress.
- `--complement` will output the complement of the selected fields. For
  example, if we had a 5 column csv data file, `cut -d ',' -f '2-4'
  --complement` would output the 1st and 5th columns.

### awk

`awk` is a pattern scanning and processing language. It is typically used to
parse files in a similar manner to using `grep` combined with `cut`. `awk` is
very powerful but we will only cover some very basic operations.

- `awk '/regexp/{print}' filename` will output all lines in `filename`
  containing `regexp`. As with `grep`, regular expressions can be used in
  `regexp`.
- `awk '/regexp/{print $1" "$3}' filename` will output the first and third
  words in all lines containing `regexp`. Note by default `awk` uses spaces as
  the field delimiter.
- `awk 'BEGIN{i=0} /regexp/{i=i+1} END{print i}' filename` will output the
  number of lines in `filename` containing `regexp`.
- `awk '/searchtext/{printf "%f %f\n",$2-13.0,$4*10.0}' filename` will output
  for each line containing `searchtext`, the second field with 13.0 subtracted,
  and the fourth field times 10.

Hopefully these examples give you an idea of what is possible with `awk`. More
details and examples can be found with `man awk`.

#### Useful Options

- `-F` allows you to set the field separator. For example `awk -F','` would be
  useful for parsing a csv file.
- `-f program-file` tells `awk` to run the commands listed in `program-file`.
  This is useful if you have a complicated script so you don't need to type it
  all in directly to the terminal.

### sed

`sed` stands for stream editor. It allows you to perform basic text
transformations on a file (or from stdin). `sed` is very powerful and we will
only cover some very simple examples.

- `sed 's/regexp/replacement/' filename > newfile` will replace the first match
  of `regexp` on each line of `filename` with `replacement`. Here the first `s`
  stands for substitute, and is probably the most useful sed command. Note that
  `sed` outputs to stdout by default, so you should redirect this to a *new*
  file to save. **Do not try to redirect output to the same file you are
  reading**, as `>` will blank the file before `sed` can read anything from it.
    - `sed 's/^...//' filename > newfile` will remove the first three
      characters from every line of `filename`. Note `^` is used to match the
      beginning of a line.
- `sed 's/regexp/replacement/g' filename > newfile` will replace *every* match
  of `regexp` on each line of `filename` with `replacement`.
    - `sed 's/,/\t/g' data.csv > data.dat` would replace all commas with tabs
      (`\t` is a tab) in `data.csv` and save it in `data.dat`.
- The `-i` flag can be used to modify a file in-place.
  `sed -i 's/regexp/replacement/g' filename` will replace every match of
  `regexp` on each line of `filename` with `replacement`. `filename` itself
  will be modified. You can also specify a suffix here such that a backup
  will be created using that suffix: e.g.
  `sed -i.bak 's/regexp/replacement/g' filename` will do the replacement in-place
  but first backup the original file to `filename.bak`.

See `man sed` for more information.

### tr

`tr` is used to translate or delete characters. It always reads from stdin, and
outputs to stdout. This means that to use it with a file, we need to redirect
the file to stdin using `<`.

- `tr 1 2 < test.dat` would output the contents of `test.dat` with all the 1s
  replaced by 2s.
- `tr abc ABC < test.txt` would output the contents of `test.txt` with any 'a'
  replaced by 'A', 'b' by 'B' and 'c' by 'C'.

It also accepts some special input such as

- `[:space:]` to match whitespace (both single and continuous).
- `[:punct:]` to match punctuation
- `[:lower:]` to match lower case letters
- `[:upper:]` to match upper case letters

For example:

- `tr [:lower:] [:upper:] < test.txt > test_upper.txt` would to create a new
  version `test.txt` converted to uppercase.
- `tr [:space:] '\n' < test.txt` would convert all spaces to newlines.

#### Useful options

- `-d` deletes matching characters. For example, to output a file with all
  punctuation removed we could do `tr -d [:punct:] < test.txt`

### find

`find` is used to search for files in a directory hierarchy. Most commonly this
is used with the `-name` option to search for files with a particular name.
Wildcards can be used in the search. Note: the first argument to `find` should
be the path to search, e.g. `find /etc` to search for files in the `/etc`
directory or `find .` to search for files in the current directory.

- `find . -name "*.cpp"` will find all files ending in `.cpp` in the current
  directory (`.`) and its subdirectories.

See `man find` for more information.

### diff

`diff` is used to compare two files. This is useful if for example, you want to
see what changes have been made in a new version of a file.

- `diff file1 file2` will output the lines which differ between the two files.
  The lines from `file1` will be prepended with `< ` and the lines from `file2`
  with `> `.

#### Useful Options

- `-q` will report only whether the two files differ and will not output the
  differences.
- `-r` will recursively compare files in subdirectories.
- `-y` will output the two files side by side in two columns.
- `-W` will allow you to set how wide the output is (130 columns by default).
  This is particularly useful with the `-y` option.

### sort

`sort` is used to sort lines of text files. For example, if we had a file
called `users.txt` which contained a list of names, then `sort users.txt` would
output (to stdout) the list sorted alphabetically. This is often useful
combined with other commands. For example to generate a sorted list of all
words in a file you can do `sed 's/ /\n/g' filename | sort`. Here the `sed`
command replaces all spaces with new lines, so we have one word per line, and
then we use this as input to the sort command.

#### Useful Options

- `-n` will sort numerically rather than alphabetically. For example,
  `du -s * | sort -n` will generate a listing of files and directories sorted
  by size.
- `-h` will use a human numeric sort, allowing numbers such as 2K and 1G to be
  sorted. For example, `du -sh * | sort -h` will generate a listing of files
  and directories sorted by size, but in human readable format.
- `-u` will output only the first of an equal run. For example
  `sed 's/ /\n/g' filename | sort -u` will generate a sorted list of all words
  in `filename` with each listed only once.
- `-f` will till `sort` to fold lower case to upper case characters.
- `-r` will output in reverse order. This is useful for numeric sorts where you
  often want to have the largest numbers at the top.

### uniq

`uniq` is used to report or omit repeated lines in a file. By default it will
take any file or input from stdin, and output it with duplicated lines omitted.
For example if we had a text file `test.txt` with

```text
a
a
b
b
b
c
d
d
```

Running `uniq test.txt` would output

```text
a
b
c
d
```

#### Useful Options

- `-c` prefixes each line of output with a count of its number of occurrences.
  We could for example, take the `sort` example to sort all words in a file,
  and expand it to generate a word count of all words in a file:
  `sed 's/ /\n/g' filename | sort | uniq -c`. We could add `| sort -n` to the
  end of this to sort words in order of frequency, and we could use `tr` before
  this to remove all punctuation.
- `-i` tells `uniq` to ignore differences in case when comparing.

### tar

`tar` is an archiving utility used to create an archive of files, i.e. generate
a file containing many other files. This is usually used to create compressed
bundles of files on Linux, in a similar way to zip file archives (note zip and
unzip are usually available on Linux also, but compressed tar archives are more
commonly used).

#### Creating Archives

The `-c` flag indicates to `tar` that you want to create an new archive.

- `tar -cvf archive.tar file1 file2 dir1` will create an (uncompressed) archive
  called `archive.tar` of the named set of files or directories. Here:
    - `-v` is for verbose mode - it will list the files which are added. This
      is not really necessary, but is useful so you can be sure you are adding
      the files you intended to.
    - `-f` is used to specify the archive name. Here we have called it
      `archive.tar`.
- `tar -czvf archive.tar.gz file1 file2 dir1` uses the additional `-z` flag to
  compress the archive using `gzip` compression. The extension `.tar.gz` or
  `.tgz` is typically used to indicate this type of file. Several other
  possible compression algorithms could be used instead:
    - `-j` will use `bzip2` compression. This typically results in slightly
      smaller file size than `gzip`, but can take slightly longer. Files
      created like this usually use the extension `.tar.bz` or `.tbz` or some
      other similar variation.
    - `-J` will use `xz` compression. This is a very effective compression
      algorithm, resulting in very small files, particularly for archives
      containing a lot of text. This option can take quite a lot longer than
      gzip for large archives however. Files created with this option usually
      use the extension `.tar.xz` or `.txz`.

#### Extracting Archives

The `-x` flag indicates to `tar` you want to unpack an archive.

- `tar -xvf archivename` will uncompress and unpack a tar archive called
  `archivename`, automatically detecting what kind of compression was used (if
  any). Again the `-v` isn't necessary, but is useful.

#### Listing Archive Content

The `-t` flag will tell `tar` to list the contents of an archive.

- `tar -tvf archivename` will list the contents of the tar archive
  `archivename`, again automatically detecting what kind of compression was
  used.

### bc

`bc` is an arbitrary precision calculator language. This can be used from
the command line by piping expressions to it. For example:

- `echo "2+2" | bc`
- For floating point operations, you should set the scale which defines how
  many digits following the decimal points are output:
  - `echo "scale=10; 1.412*27.211" | bc`
  - `echo "scale=10; sqrt(2)" | bc`
- You can tell bc to load the standard math library with the `-l` flag. This
  will also set the scale to 20 by default. This makes several additional
  functions available such as `s(x)`, `c(x)` and `a(x)` for the sine, cosine
  and arctan in radians. So `echo "4*a(1)" | bc -l` will output the first
  pi to decimal places.

Job Control
-----------

Linux allows jobs to run both in the foreground and background of a terminal
session. If we launch a program from the terminal by typing e.g. `gedit` and
pressing enter (`gedit` is a text editor), then we can't can't do anything else
in the terminal until `gedit` is closed. `gedit` is said to be running in the
foreground in this case.

With `gedit` running in the foreground, we can move it to the background as
follows.

1. In the terminal where it is running, press `ctrl+z`. This stops the job. A
   message will appear in the terminal like
   `[1]+  Stopped gedit`. If you try to type, or click any menu buttons, on
   `gedit` it won't respond.
2. Type `bg` in the terminal. This moves the most recent stopped job to the
   background, i.e. `gedit` in this example. Now you'll be able to use both
   `gedit` and the terminal as normal.

- To move a job that's running in the background, back to the foreground you
  can use the `fg` command.
- To start a job in the background you can append `&` to the command when you
  enter it in the terminal. For example `gedit &` will launch `gedit` in the
  background directly.

